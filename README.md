# GameVANIR #



### What is GameVANIR? ###

**GameVANIR** (**V**erifiable **A**ccess to **N**FTs **I**nstilled with **R**eality) is a gaming-related **NFT** ecosystem consisting of art collections, marketplaces, and **GameFi** services.